var path = require('path');
 var webpack = require('webpack');
 module.exports = {
     entry: './src/index.js',
     output: {
         path: path.resolve(__dirname, 'build'),
         filename: 'app.bundle.js'
     },
     module: {
         rules: [
             {
                 test: /\.js$/,
                 loader: 'babel-loader',
                 query: {
                    "presets": ["@babel/env", "@babel/react"]
                 }
             },
             {
                test: /\.css$/,
                loader: "css-loader" // translates CSS into CommonJS
             }
        ]              
     },
     stats: {
         colors: true
     },
     devtool: 'source-map'
 };