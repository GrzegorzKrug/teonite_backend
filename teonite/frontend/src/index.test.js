import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import reducer from "./reducers/reducer";
import rootSaga from "./sagas/sagas";

import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import { BrowserRouter, Route  } from "react-router-dom"

import "./styles/styles.css"

const sagaMiddleware = createSagaMiddleware();
const store = createStore(reducer, applyMiddleware(sagaMiddleware));

// sagaMiddleware.run(rootSaga);

it('Render test', () => {
	const div = document.createElement('div');
	ReactDOM.render(
	  <Provider store={store}>
	  	<BrowserRouter>
		  	<Route  path='/stats'>  	  
		  		<App/>
		  	</Route>
	  	</BrowserRouter>
	  </Provider>,
	  div
	);
	ReactDOM.unmountComponentAtNode(div);
})

