import React, { Component } from "react";
import { connect } from "react-redux";


class ButtonToggleSeparate extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(button_value) {          
    this.props.onToggleStatsTable()    
  }  

  render() {  	    
  	return (    
  		  <button onClick={this.handleChange}>        
          Toggle main table   
        </button>          
    )
  }
}

const mapStateToProps = state => {
  return {state: state}
};

const mapDispachToProps = dispatch => {
  return {
  onToggleStatsTable: () => dispatch({ type: "TOGGLE_MAIN_TABLE"}), 
  };
};


export default connect(  
  mapStateToProps,
  mapDispachToProps
)(ButtonToggleSeparate);
