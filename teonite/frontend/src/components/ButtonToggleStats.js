import React, { Component } from "react";
import { connect } from "react-redux";


class ButtonToggleStats extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(button_value) { 

    if (!this.props.show_stats_table){ // fetching stats before button state change
      this.props.onGetStats()  
    }
    this.props.onToggleStatsTable()    
  }  

  render() {  	    
  	return (    
  		  <button onClick={this.handleChange}>        
          Toggle All words   
        </button>          
    )
  }
}

const mapStateToProps = state => {
  return {show_stats_table: state.show_stats_table}
};

const mapDispachToProps = dispatch => {
  return {
  onToggleStatsTable: () => dispatch({ type: "TOGGLE_STATS_TABLE"}), 
  onGetStats: () => dispatch({ type: "GET_STATS"}),
  };
};


export default connect(  
  mapStateToProps,
  mapDispachToProps
)(ButtonToggleStats);
