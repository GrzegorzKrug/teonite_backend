import React, { Component } from "react";
import { connect } from "react-redux";

const TableHead = ({data}) => {
  // var header = data.header
  return(
    <thead>
      <tr>      
        <td key="word_header">Word</td>
        <td key="count_header">Count</td>      
      </tr>
    </thead>
  )
}

const TableRows = ({data}) => {
    let words = data    
    let tbody = []
    
    for (var x of words){           
      tbody.push(
        <tr key={x.id}>
          <td>{x.id}</td>
          <td>{x.label}</td>
        </tr>
      )  
    }    
    return tbody    
}


const MyTable = ({data}) => {return(
  <table className="table_stats"> 
      <TableHead/>    
      <tbody>
        <TableRows data={data} />    
      </tbody>
  </table>
  
)}

class TableWordsStats extends Component {    

  render() {   
    if (this.props.show_stats_table){
      return (  
        <div>
          <h1>All words table</h1>
          <MyTable data={this.props.stats} />
        </div>
      )
    }
    else{
      return (null);
    }  
  }
}


const mapStateToProps = state => {
  return {
  stats: state.stats,
  hasError: state.hasError,
  show_stats_table: state.show_stats_table,
  };
};

const mapDispachToProps = dispatch => {
  return {
  onSort: () => dispatch({ type: "SORT_ALL_WORDS",}),  
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps,
)(TableWordsStats);

