import React, { Component } from "react";
import { connect } from "react-redux";


 class ErrorBoundary extends Component {
    constructor(props) {
       super(props);
       this.state = { hasError: false };
       this.reload_app = this.reload_app.bind(this);
    }

    static getDerivedStateFromError(error) {
       // Update state so the next render will show the fallback UI.
       return { hasError: true };
    }

    componentDidCatch(e) {
      console.log("Error cached in ErrorBoundary.js", e)
    }

    reload_app(){
        // Reloading Authors list and clearing tables.
        this.props.onGetAll()
        this.props.onClear()  
        this.props.onClearSelection()        
        this.setState({hasError: false})
    }

    render() {
      if (this.state.hasError) {        
        return (
            <div>
                <button onClick={this.reload_app}>Reset tables</button>
                <h1>Error has occured. Press button to fix.</h1>
                Probably table can not render 'undefined' values, database content could be changed.
                Check console for more info.                
            </div>
        );
      }
      else{
        return this.props.children;   
      }
      
    }
 }

const mapStateToProps = state => {
  return {
  state: state
  };
};

const mapDispachToProps = dispatch => {
  return {
  onGetAll: () => dispatch({ type: "GET_ALL",}),  
  onClear: () => dispatch({ type: "CLEAR_WORDS",}),
  onClearSelection: () => dispatch({ type: "CLEAR_SELECTION",}),
  
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(ErrorBoundary);

