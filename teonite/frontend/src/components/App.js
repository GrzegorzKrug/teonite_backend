import React, { Component } from "react";
import { connect } from "react-redux";

import TableSeparateAuthors from './TableSeparateAuthors'
import TableSumWords from './TableSumWords'
import TableWordsStats from './TableWordsStats'

import ButtonToggleSeparate from './ButtonToggleSeparate'
import ButtonToggleStats from './ButtonToggleStats'
import ButtonToggleSum from './ButtonToggleSum'

import MultiSelect from "@kenshooui/react-multi-select";
import "@kenshooui/react-multi-select/dist/style.css"

import ErrorBoundary from './ErrorBoundary'

class App extends Component {
  	constructor(props) {
		super(props);
		this.handleChange = this.handleChange.bind(this);		
  	}


	handleChange(selectedItems) {	
		this.props.onSelected(selectedItems)		
		this.props.onFetchAuthors(selectedItems);			
	}  	

	componentDidMount(){
		// Load list first time
		this.props.onGetAll()
	}	

	render() {
		const { authors, selectedItems } = this.props;		
		return (
			<div><ErrorBoundary>
				<button onClick={this.props.onGetAll}>Reaload List</button> 
				<ButtonToggleSeparate/>
				<ButtonToggleStats/>
				<ButtonToggleSum/>
				<MultiSelect
        			items={authors}
        			selectedItems={selectedItems}
        			onChange={this.handleChange}        			
        		/>        		
        		<TableSeparateAuthors/>        		
        		<TableWordsStats/>
        		<TableSumWords/>				
			</ErrorBoundary></div>
		);
	}
}


const mapStateToProps = state => {
  return {
	authors: state.authors,
	selectedItems: state.selectedItems,
	table: state.table,
  };
};

const mapDispachToProps = dispatch => {
  return {
	onGetAll: () => dispatch({ type: "GET_ALL",}),		
	onSelected: (selection) => dispatch({ type: "SELECTED", value: selection}),	
	onFetchAuthors: (selection) => dispatch({ type: "FETCH_SELECTED_AUTHORS", value: selection}),
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps
)(App);
