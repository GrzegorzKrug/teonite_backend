import React, { Component } from "react";
import { connect } from "react-redux";
// import { StyleSheet, Text, View } from 'react-native-web';

const TableHead = ({data}) => {  
  var table_head = []
  // Pushing Author Names and empty cell
  data.header.map(author => {
          table_head.push(<td key={author.id}>{author.label}</td>)
          table_head.push(<td className="123" key={author.id+"_temp_key"}></td>)
          return null
        })

  return(
    <thead>
      <tr>
        {table_head}   
      </tr>
    </thead>
  )
}

const TableRows = ({data}) => {        
    var content = data.content   
    let tbody = []
    let row = []

    // Pushing "Word" and "Count" cells
    for (let author_id = 0; author_id < content.length; author_id++) {
        row.push(<td key={author_id+"_word_header_"}>Word</td>)
        row.push(<td key={author_id+"_count_header_"}>Count</td>)
    }
    tbody.push(
        <tr key="header">
          {row}
        </tr>
      ) 

    // Pushing words in rows from each author
    for (let word_id = 0; word_id < 10; word_id++) {            
        row = []

        for (let author_id = 0; author_id < content.length; author_id++) {
          let word = content[author_id][word_id].id
          let count = content[author_id][word_id].label

          row.push(
            <td key={author_id+"_word_"+word_id}>{word}</td>)
          row.push(
            <td key={author_id+"_count_"+word_id}>{count}</td>
          )
      }

      tbody.push(
        <tr key={word_id}>
          {row}
        </tr>
      )  
    }
    return <tbody>{tbody}</tbody>
}


const MyTable = ({data}) => {return(  
  <table className="separate_table">    
      <TableHead data={data} />    
      <TableRows data={data} />    
  </table>
  
)}

class TableSeparateAuthors extends Component {
  render() {       
    if (this.props.show_separate_tables){
        return ( 
          <div> 
            <h1>Authors tables</h1>  
            <MyTable data={this.props.separate_table}/>
          </div>
        )
    }
    else{
      return (null);
    }  
  }
}

const mapStateToProps = state => {
  return {
  separate_table: state.separate_table,  
  show_separate_tables: state.show_separate_tables,
  };
};

export default connect(
  mapStateToProps,
)(TableSeparateAuthors);
