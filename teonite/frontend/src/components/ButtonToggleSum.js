import React, { Component } from "react";
import { connect } from "react-redux";


class ButtonToggleSum extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(button_value) {          
    this.props.onToggleSumTable()    
  }  

  render() {  	    
  	return (    
  		  <button onClick={this.handleChange}>        
          Toggle sum   
        </button>          
    )
  }
}

const mapStateToProps = state => {
  return {state: state}
};

const mapDispachToProps = dispatch => {
  return {
  onToggleSumTable: () => dispatch({ type: "SHOW_HIDE_SUM_TABLE"}), 
  };
};


export default connect(  
  mapStateToProps,
  mapDispachToProps
)(ButtonToggleSum);