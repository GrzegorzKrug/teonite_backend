import React, { Component } from "react";
import { connect } from "react-redux";


const TableHead = ({data}) => {  
  return(
    <thead>
      <tr>      
        <td key="word_header">Word</td>
        <td key="count_header">Count</td>      
      </tr>
    </thead>
  )
}

const TableRows = ({data}) => {
    let words = data    
    let tbody = []
    
    for (var x of words){           
      tbody.push(
        <tr key={x.id}>
          <td>{x.id}</td>
          <td>{x.label}</td>
        </tr>
      )  
    }    
    return tbody    
}


const MyTable = ({data}) => {return(
  <table className="table_sum_words">    
      <TableHead/>    
      <tbody>
        <TableRows data={data} />    
      </tbody>
  </table>
  
)}

class TableSumWords extends Component {
    
  componentDidUpdate() {  
    // Sorting
    this.props.onSort()
  }

  render() {   
    if (this.props.show_sum_tables){

        return (  
          <div> 
            <h1>Sum Table</h1> 
  		      <MyTable data={this.props.all_words} />
          </div>
        )
    }
    else{
      return (null);
    }  
  }
}


const mapStateToProps = state => {
  return {
  all_words: state.all_words,  
  show_sum_tables: state.show_sum_tables,
  };
};

const mapDispachToProps = dispatch => {
  return {
  onSort: () => dispatch({ type: "SORT_ALL_WORDS",}),  
  };
};

export default connect(
  mapStateToProps,
  mapDispachToProps,
)(TableSumWords);