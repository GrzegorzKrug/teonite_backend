import reducer from "./reducer"


it('Testing cases', () => {
	assert(null == reducer(null, {}))
	assert(null != reducer(undefined, {}))

	let state = reducer(undefined, {})
	
	assert("authors" in state)
	assert("selectedItems" in state)
	assert("separate_table" in state)
	assert("stats" in state)
	assert("show_separate_tables" in state)
	assert("show_sum_tables" in state)
	assert("show_stats_table" in state)
})

it('Testing reducer "SET_AUTHORS"', () => {
	try{
		let state = reducer(undefined, {type: "SET_AUTHORS"})
	}
	catch(error){
		if (error instanceof TypeError) {
  		} else {
    		throw e;  // re-throw the error unchanged
  		}
	}
})


it('Testing reducer "NEW_STATS"', () => {
	try{
		let state = reducer(undefined, {type: "NEW_STATS"})
	}
	catch(error){
		if (error instanceof TypeError) {
  		} else {
    		throw e;  // re-throw the error unchanged
  		}
	}
})

it('Testing reducer "UPDATE_WORDS"', () => {
	try{
		let state = reducer(undefined, {type: "UPDATE_WORDS"})
	}
	catch(error){
		if (error instanceof TypeError) {
  		} else {
    		throw e;  // re-throw the error unchanged
  		}
	}
})
it('Testing reducer "CLEAR_WORDS"', () => {

	let state = reducer(undefined, {type: "CLEAR_WORDS"})
	assert(state.all_words.length == 0, "Empty Array length")
	assert(state.separate_table.header.length == 0, "Empty Array length")
	assert(state.separate_table.content.length == 0, "Empty Array length")	
})

it('Testing toggle function for main table', () => {
	let state = reducer(undefined, {type: "TOGGLE_MAIN_TABLE"})
	assert(state.show_separate_tables, "Display separate authors")
	
	state = reducer(state, {type: "TOGGLE_MAIN_TABLE"})
	assert(!state.show_separate_tables, "Display separate authors")
})

it('Testing toggle function for sum table', () => {
	let state = reducer(undefined, {type: "SHOW_HIDE_SUM_TABLE"})
	assert(state.show_sum_tables, "Display separate authors")
	
	state = reducer(state, {type: "SHOW_HIDE_SUM_TABLE"})
	assert(!state.show_sum_tables, "Display separate authors")
})

it('Testing toggle function for stats table', () => {
	let state = reducer(undefined, {type: "TOGGLE_STATS_TABLE"})
	assert(state.show_stats_table, "Display separate authors")
	
	state = reducer(state, {type: "TOGGLE_STATS_TABLE"})
	assert(!state.show_stats_table, "Display separate authors")
})

function assert(condition, message) {
    if (!condition) {
        throw message || "Assertion failed";
    }
}
