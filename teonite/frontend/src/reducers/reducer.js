
const initialState = {
  authors: [],
  selectedItems: [],
  separate_table: {header: [], content: []},
  all_words: [],
  stats: [],
  // hasError: false,
  show_separate_tables: false,
  show_sum_tables: false,
  show_stats_table: false,  

};

const reducer = ( state = initialState, action) => {
  const newState = { ...state };
  let proto_obj

  switch (action.type) {         

    case 'NEW_STATS':  
        proto_obj = action.value
        var words = Object.keys(proto_obj).map(function (key) {
            return {id: key, label: proto_obj[key]}; 
        }); 
        newState.stats = words
        return newState       
                   
    case 'SET_AUTHORS':                      
        // Seting available authors        
        proto_obj = action.value
        var authors = Object.keys(proto_obj).map(function (key) {
            return {id: key, label: proto_obj[key]}; 
        });
        
        newState.authors = authors
        return newState

    case 'UPDATE_WORDS':   
        // Adding words togheter      
        proto_obj = action.value;             
        var old_words = {}      
 
        // ========= Converting words fetched by saga        
        var action_words = Object.keys(proto_obj).map(function (key) {
            return {id: key, label: proto_obj[key]}; 
        });   

        // ========= SEPARATE TABLE CONTENT
        var new_table = {
            header: [...newState.separate_table.header, action.author_obj],
            content: [...newState.separate_table.content, action_words],
        }
        newState.separate_table = new_table

        // ========= Converting array to object        
        for (let x of newState.all_words){            
            old_words[x.id] = x.label
        }

        // ========= Adding words to object
        for (let x of action_words){
            if(x.id in old_words){
                old_words[x.id] = old_words[x.id] + x.label  
            } 
            else{
                old_words[x.id] = x.label  
            }
        }

        var all_words = []        
        for (let x in old_words){
            all_words.push({id: x, label: old_words[x]})
        }   
             
        newState.all_words = all_words        
        return newState

    case 'CLEAR_WORDS':
        // Clearing Tables
        newState.all_words = []
        newState.separate_table = {header: [], content: []}        
        return newState

    case 'CLEAR_SELECTION':
        newState.selectedItems= []
        return newState

    case 'TOGGLE_MAIN_TABLE':    
        newState.show_separate_tables = !newState.show_separate_tables
        return newState
 
    case 'SHOW_HIDE_SUM_TABLE':
        // Toggle table visibility        
        newState.show_sum_tables = !newState.show_sum_tables
        return newState

    case 'TOGGLE_STATS_TABLE':
        // Toggle table visibility        
        newState.show_stats_table = !newState.show_stats_table
        return newState
        
    case 'SORT_ALL_WORDS':       
        // Sorting Words from all authors 
        newState.all_words.sort((a, b) => {            
            return b.label - a.label;
        })            
        return newState

    default:
      	return state
  }
    	
}



export default reducer;