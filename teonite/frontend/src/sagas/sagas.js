import axios from 'axios'
import {put, takeLatest, call, all } from 'redux-saga/effects'


export function* getAll() { 
  const url = `http://localhost:8080/authors/`;
  try {
      let { data } = yield call(axios.get, url);
      yield put({type: 'SET_AUTHORS', value: data});

    } catch(error) {
      console.log('Cached error in sagas.getAll: ', error)
    }
}

export function* getStats(){
  const url_stats = `http://localhost:8080/stats/`;
  try {
      let { data } = yield call(axios.get, url_stats);
      yield put({type: 'NEW_STATS', value: data});

    } catch(error) {
      console.log('Cached error in sagas.getStats: ', error)
    }

}


export function* getOneAuthor(author_obj) {   
  const url = `http://localhost:8080/stats/` + author_obj['id'] + '/';
  try {      
      let { data } = yield call(axios.get, url);
      yield put({type: 'UPDATE_WORDS', value:data, author_obj: author_obj})

    } catch(error) {
      console.log('Cached error in sagas.getOneAuthor: ', error)
    }
}


export function* getSelectedAuthors(action) {
  var authors = action.value  
  
  yield put({type:'CLEAR_WORDS'});

  yield all(authors.map(author => {    
    return call(getOneAuthor, author)
  }))  
}


export default function* rootSaga() {
  yield takeLatest('GET_ALL', getAll)
  yield takeLatest('FETCH_SELECTED_AUTHORS', getSelectedAuthors)
  yield takeLatest('GET_STATS', getStats)
}