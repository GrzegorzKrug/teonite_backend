import { getSelectedAuthors, getOneAuthor, getStats, getAll } from "./sagas"
import {put, takeLatest, call, all } from 'redux-saga/effects'


it('Simple Call', () => {
	getSelectedAuthors(),
	getOneAuthor(),
	getStats(),
	getAll()
})


it('Test fetching all authors', () => {
	// assert.deepEqual()
	const gen = getAll();
	let response = gen.next()

	assert(response.value.type == "CALL", "saga should return 'CALL'")		
	assert(response.value.payload.args == `http://localhost:8080/authors/`, 'Url does not match')
})

it('Test fetching stats', () => {
	// assert.deepEqual()
	const gen = getStats();
	let response = gen.next()

	assert(response.value.type == "CALL", "saga should return 'CALL'")		
	assert(response.value.payload.args == `http://localhost:8080/stats/`, 'Url does not match')
})

it('Test fetching one author only', () => {
	// assert.deepEqual()
	let test_obj = {id: "johnrambo"}
	const gen = getOneAuthor(test_obj);

	let url = 'http://localhost:8080/stats/johnrambo/'
	let response = gen.next()

	assert(response.value.type == "CALL", "saga should return 'CALL'")		
	assert(response.value.payload.args == url, 'Url does not match')
})

it('Test fetching selected authors', () => {
	
	let test_action = {value: [{id: "johnrambo"}, {id: "brucewayne"}]}
	const gen = getSelectedAuthors(test_action);

	let response = gen.next()
	assert(response.value.type == "PUT")
	assert(response.value.payload.action.type == "CLEAR_WORDS")

	response = gen.next()	
	assert(response.value.type == "ALL")
	assert(response.value.payload.length == 2)
})


function assert(condition, message) {
    if (!condition) {
        throw message || "Assertion failed";
    }
}