import socket
import time
import os
import django

port = int(os.getenv('DB_PORT', '5432')) # 5432

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
while True:
    try:
        print("WAITING FOR DB!")
        s.connect(('db', port))
        s.close()
        break
    except socket.error as ex:
        time.sleep(5)
    except django.db.utils.OperationalError as OE:
        print("OperationalError Occured!")
        time.sleep(5)

print("DB IS ALIVE!")
