from django.db import models
from django.core.validators import MinValueValidator
import uuid
# Create your models here.


class Word(models.Model):
    id_key = models.UUIDField(primary_key=True,
                              default=uuid.uuid4, editable=False)
    user_alias = models.CharField(max_length=200)
    user_name = models.CharField(max_length=200)
    count = models.IntegerField(validators=[MinValueValidator(1)])
    word = models.CharField(max_length=200)

# class User(models.Model):
#     alias = models.CharField(max_length=200)
#     name = models.CharField(max_length=200)
