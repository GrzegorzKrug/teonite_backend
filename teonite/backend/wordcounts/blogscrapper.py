import requests
import bs4
import re
from stop_words import get_stop_words


class BlogScrapper:
    def __init__(self):
        self.teonitepage = r'https://teonite.com/'
        self.regex_blogUrl = re.compile('^.*(blog.*$)')
        self.blog_list = []

    def read_teonite_page(self):
        # Read Teonite Blog Section and return Blog list
        next_page = self.teonitepage + r'blog/'  # This is starting page
        self.blog_list = []  # clear list before adding new
        while next_page:  # loop if next pages returned
            blogs, next_page = self.search_blog_urls(next_page)
            self.blog_list += blogs  # add 2 lists

        return self.blog_list

    def search_blog_urls(self, page_url: str):
        # This method finds blog urls and url of next page
        r = requests.get(page_url)
        r.encoding = 'utf8'
        r.raise_for_status()  # Built-in: check if response is 200
        soup = bs4.BeautifulSoup(r.text, "html.parser")  # convert to Soup

        # Searching urls to blogs
        soup_title = soup.find_all(attrs={'class': 'post-title'})
        blog_list = []
        for element in soup_title:
            for c in element.children:
                link = c['href']
                blogUrl = self.regex_blogUrl.search(link).group(1)
                blog_list.append({
                    'title': element.text,
                    'url': self.teonitepage + blogUrl
                })

        # searching next page
        soup_button = soup.find_all(
            attrs={'class': 'blog-button post-pagination'})
        buttons = []
        for element in soup_button:
            for c in element.children:
                if c.name:  # Skip navigateble strings
                    buttons.append(
                        self.teonitepage
                        + self.regex_blogUrl.search(c['href'])
                        .group(1)
                    )

        # find next page number
        next_page = None
        try:
            page_num = int(re.findall(r'\d+', page_url)[0])
        except IndexError:  # handling page without number
            page_num = 0

        if len(buttons) > 2:
            raise ValueError('Got more buttons than 2')

        for bt in buttons:
            try:
                but_num = int(re.findall(r'\d+', bt)[0])
                if but_num > page_num:
                    next_page = bt
                    break
            except IndexError:  # handling button without number page
                pass

        return blog_list, next_page

    def blogscrapper(self, blog_url: str):
        # This method reads title, tags, author and text from blog
        regex_text = re.compile(r'.*?>(.*?)<.*?')
        r = requests.get(blog_url)
        r.encoding = 'utf8'
        r.raise_for_status()

        soup = bs4.BeautifulSoup(r.text, 'html.parser')

        # Find Title in soup
        result = soup.find_all(attrs={'class': 'post-title'})
        title = re.findall(regex_text, str(result))
        if not title == []:
            title = title[0]
        else:
            title = None

        # Find Tags in soup
        result = soup.find_all(attrs={'class': 'tags-list'})
        tags = re.findall(regex_text, str(result))

        # Find Content (text) in soup
        result = soup.find_all(attrs={'class': 'post-content'})
        textList = re.findall(regex_text, str(result))
        textList = [cell for cell in textList if len(cell) > 0]
        counter = self.count_words(textList)

        # Find author in soup (or more authors)
        result = soup.find_all(attrs={'class': 'author-name'})
        author = re.findall(regex_text, str(result))
        author = [cell for cell in author if len(cell) > 0]

        return {'author': author,
                'words': counter,
                'title': title,
                'tags': tags
                }

    def count_words(self, textList: list):
        # Count words from this textList
        bad_strings = get_stop_words('english') + \
            get_stop_words('polish')

        text = ' '.join(textList)  # Create string for regex
        words = re.findall(r'[A-Za-z]+', text)
        words = [w for w in words if (w not in bad_strings) and len(w) > 1]

        counter = {}
        for word in words:
            counter.update({word.lower(): counter.get(word.lower(), 0) + 1})
        return counter

    def count_all_words(self, dataDict: dict):
        # Add new author 'all' and count words from rest authors
        for author, values in dataDict.items():
            if author == 'all':
                continue

            word_counter = {}
            for word, count in values['words'].items():
                word_counter.update({word: values['words']
                                     .get(word, 0)
                                     + count
                                     })
        return dataDict
    # Sorting in DB
    # def sort_words(self, dataDict: dict):
    #     # Sorting words in dataDict, not used anymore
    #     new_dict = {}
    #     for key, value in dataDict.items():
    #         new_dict[key] = value
    #         sorted_list = list(value['words'].items())
    #         sorted_list.sort(key=lambda x: x[1], reverse=True)
    #         new_dict[key]['words'] = sorted_list
    #     return new_dict

    def normalize_char(self, c: 'one letter'):
        # This Method converts polish chars to ascii
        if c == 'ć':
            return 'c'
        elif c == 'ą':
            return 'a'
        elif c == 'ę':
            return 'e'
        elif c == 'ł':
            return 'l'
        elif c == 'ó':
            return 'o'
        elif c == 'ź':
            return 'z'
        elif c == 'ż':
            return 'z'
        elif c == 'ś':
            return 's'
        elif c == 'ć':
            return 'c'
        else:
            return c

    def change_name_to_alias(self, dataDict: dict):
        new_dict = {}
        for key, value in dataDict.items():
            alias = ''.join(key.lower().split())  # lowercase + no spaces
            alias = ''.join(self.normalize_char(c) for c in alias)

            new_dict.update({alias: value})
            new_dict[alias]['name'] = key

        return new_dict

    def run_counting(self) -> "dict{'<user_alias>': dict{" \
        "'name': str," \
        "'words':dict{<word>: int}" \
            "}}":
        # Collecting dicts of word count, adding them togheter
        blog_list = self.read_teonite_page()
        data = {}
        # Accumulate words from all blogs
        for blog in blog_list:
            last_blog = self.blogscrapper(blog['url'])
            # Loop if there is more authors to add
            for last_author in last_blog['author']:
                if last_author not in data:
                    data.update({
                        last_author: {'articlenums': 0, 'words': {}}
                    })

                # Accumulate words just for last_author
                word_counter = {}
                for word, count in last_blog['words'].items():
                    word_counter.update({word: data[last_author]['words']
                                         .get(word, 0)
                                         + count})

                data[last_author]['words'].update(word_counter)
                data[last_author].update({
                    'articlenums': data[last_author].get('articlenums', 0) + 1
                })
        data = self.count_all_words(data)
        data = self.change_name_to_alias(data)
        return data


if __name__ == '__main__':
    app = BlogScrapper()
    data = app.run_counting()
    input('End main...')
