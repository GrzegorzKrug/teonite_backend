from django.db import connection
from celery import shared_task
from .models import Word
from .blogscrapper import BlogScrapper
import threading

@shared_task
def auto_fill_table():
    data = get_data()
    clear_table()
    fill_table(data)
    return True


@shared_task
def get_data(*args, **kwargs):
    app = BlogScrapper()
    data = app.run_counting()
    return data


@shared_task
def fill_table(data):
    threads = []
    for alias, values in data.items():
        th = threading.Thread(target=table_insert_thread, args=(alias, values,))
        threads.append(th)
        th.start()

    for th in threads:
        th.join()

    return True


def table_insert_thread(alias, values):
    for word, count in values['words'].items():
        record = Word(user_alias=alias, user_name=values['name'],
                      count=count, word=word)
        record.save()


@shared_task
def clear_table(*arsg, **kwargs):
    # Request for clearing table
    cursor = connection.cursor()
    cursor.execute('DELETE FROM wordcounts_word;')
    return True
