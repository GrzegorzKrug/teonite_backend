from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.http import JsonResponse
from django.db import connection
from django.db.models import Sum
from rest_framework.views import APIView
from .models import Word
import os
import re


class HomeView(APIView):
    def get(self, request, format=None):
        return HttpResponse("Application is running <br>"
                            + "Nothing is here, you should check "
                            + r'<a href="http://127.0.0.1:8080/hello/">'
                            + r'Hello Page</a>')


class HelloView(APIView):
    def get(*args, **kwargs):
        # Hello there :D
        im_path = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                               os.path.join('gifs', 'hello_there.gif')
                               )
        with open(im_path, "rb") as f:
            return HttpResponse(f.read(), content_type="image/gif")


class AuthorsView(APIView):
    def get(*args, **kwargs):
        # This method shows user names and their alias
        table = Word.objects.values_list('user_alias', 'user_name').distinct()

        return JsonResponse(dict(table))


class StatsView(APIView):
    def get(request, *args, **kwargs):
        # This method returns top 10 used words
        table = Word.objects.values_list('word') \
            .annotate(new_count=Sum('count')) \
            .order_by('-new_count')[:10]

        return JsonResponse(dict(table))


class StatsUserView(APIView):
    def get(request, *args, **kwargs):
        # This method returns top 10 used words by specific user
        user_alias = kwargs['user_alias']
        # Get Query Table
        table = Word.objects.values_list('word') \
            .filter(user_alias=user_alias) \
            .annotate(new_count=Sum('count')) \
            .order_by('-new_count')[:10]

        return JsonResponse(dict(table))
