from django.contrib import admin
from django.urls import path, re_path
from wordcounts.views import (HomeView, HelloView,
                              StatsView, StatsUserView, AuthorsView,
                              )

from django.conf.urls import url


urlpatterns = [
    # Admin
    path('admin/', admin.site.urls),
    
    # Backend
    path('home/', HomeView.as_view()),
    path('', HomeView.as_view()),
    path('hello/', HelloView.as_view()),
    path('authors/', AuthorsView.as_view()),
    path('stats/', StatsView.as_view()),
    url(r'stats/(?P<user_alias>[a-z]+)/?', StatsUserView.as_view()),
]
