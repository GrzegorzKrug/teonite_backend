from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from django.conf import settings


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'compos_server.settings')
app = Celery('compos_server',
             include=['wordcounts.tasks',
                      ])

app.config_from_object('compos_server.settings', namespace='CELERY')
app.conf.beat_schedule = {
    'run-every-120-seconds': {
        'task': 'wordcounts.tasks.auto_fill_table',
        'schedule': 120,
    },
}
