# Readme.md

### Implemented test
- [x] webscrapping (blogscrapper module)
- [ ] test django
- [ ] test database
- [ ] test frontend

### How to run?

#### 1. Use virtual environment (optional)

`python3 -m venv venv`
##### activate it

##### 2. Install module
naviagte to Main project folder where `setup.py` is.
run `pip3 install -e .`
This will install module with editable state in your system.