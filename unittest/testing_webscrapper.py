import sys
import os
import unittest
import time
from teonite.compos_server.wordcounts.blogscrapper import BlogScrapper


class UnitTest(unittest.TestCase):
    def setUp(self):
        self.app = BlogScrapper()

    def test_incorrect_page(self):
        blog_list, next_page = self.app.search_blog_urls(
            r'https://pl.wikivoyage.org/wiki/Saltillo')
        self.assertEqual(type(blog_list), list)
        self.assertEqual(len(blog_list), 0)
        self.assertEqual(next_page, None)

    def test_good_page(self):
        blog_list, next_page = self.app.search_blog_urls(
            r'https://teonite.com/blog/page/3/index.html')

        self.assertEqual(type(blog_list), list)
        self.assertEqual(len(blog_list), 10)
        self.assertEqual(type(next_page), str)
        self.assertEqual(next_page,
                         r'https://teonite.com/blog/page/4/index.html')

    def test_scrapping(self):
        out = self.app.blogscrapper(
            r'https://teonite.com/blog/conquest-of-space-with-librespace/')

        self.assertEqual(type(out), dict)
        self.assertTrue('author' in out)
        self.assertTrue('words' in out)
        self.assertTrue('title' in out)
        self.assertTrue('tags' in out)

        self.assertEqual(type(out['title']), str)
        self.assertEqual(type(out['words']), dict)
        self.assertEqual(type(out['tags']), list)

        bad_out = self.app.blogscrapper(
            r'https://pl.wikivoyage.org/wiki/Saltillo')

    def test_read_teonite_page(self):
        out = self.app.read_teonite_page()
        self.assertEqual(type(out), list)
        self.assertTrue(len(out) >= 1)

    def test_counter(self):
        abc_list = ['hello', 'hello', 'jam', 'jam', 'jam', 'teonite', '123']
        out_dict = self.app.count_words(abc_list)
        self.assertEqual(type(out_dict), dict)
        self.assertEqual(out_dict['hello'], 2)
        self.assertEqual(out_dict['jam'], 3)
        with self.assertRaises(KeyError) as raises:
            out_dict['ghost']

    def test_polish_chars(self):
        text = "No cześć ☺, witam :)"
        out_text = ''.join(self.app.normalize_char(letter) for letter in text)
        self.assertEqual(out_text, "No czesc ☺, witam :)")

    def test_run(self):
        out = self.app.run_counting()
        self.assertEqual(dict, type(out))
        self.assertTrue('michalgryczka' in out)
        self.assertTrue('name' in out['michalgryczka'])
        self.assertTrue('words' in out['michalgryczka'])
        self.assertTrue('articlenums' in out['michalgryczka'])
        self.assertTrue('Michał Gryczka' == out['michalgryczka']['name'])

        words = out['michalgryczka']['words']
        self.assertTrue(words['poland'] >= 8)


if __name__ == '__main__':
    unittest.main()
