from setuptools import setup

setup(
    name='TeoniteBlogApplication',
    version='0.5',
    description='Collecting words from blogs posted on https://teonite.com/blog/',
    author='Grzegorz Krug',
    author_email='my mail',
    packages=['teonite'],
    install_requires=['django==3.0', 'requests==2.22.0', 'bs4==0.0.1',
					'stop_words==2018.7.23',
					'djangorestframework3.11.0', 'django-cors-headers3.2.0',
					'psycopg2==2.8.4', 'celery==4.4.0'],
)
