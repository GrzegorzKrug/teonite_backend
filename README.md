# Readme.md

# Backend

### Start project
0. Download project to you local drive
1. Navigate to `.teonite_backend/teonite` 
2. run command in shell `docker-compose up`

###  settings.py!
```
# Don't run with debug turned on in production!
DEBUG = True
```

### DB refills every ~2 mins



### Commands example

default ip is `localhost` qual to `127.0.0.1` and port `8080`

##### Top used words from all blogs

	`curl http://localhost:8080/stats/`

##### List of Authors

	`curl http://localhost:8080/authors/`

##### Top used words by specifig user

	`curl http://localhost:8080/stats/<author>`
    replace `<author>` with some user alias, check `.../authors/` page


### Tree

```

.teonite
├── LICENSE
├── README.md
├── setup.py
├── teonite
│   ├── backend
│   │   ├── compos_server # Django Server named compos_server ¯\_(ツ)_/¯
│   │   │   ├── celery.py  # Celery App
│   │   │   ├── __init__.py
│   │   │   ├── settings.py
│   │   │   ├── urls.py
│   │   │   └── wsgi.py
│   │   ├── Dockerfile
│   │   ├── manage.py
│   │   ├── requirements.txt
│   │   └── wordcounts
│   │       ├── admin.py
│   │       ├── apps.py
│   │       ├── blogscrapper.py     # Web scrapping module
│   │       ├── gifs
│   │       │   └── hello_there.gif
│   │       ├── __init__.py
│   │       ├── migrations
│   │       │   ├── 0001_initial.py
│   │       │   └── __init__.py
│   │       ├── models.py
│   │       ├── tasks.py        # Tasks defined for worker
│   │       ├── tests.py
│   │       └── views.py
│   ├── docker-compose.yml    # docker-compose up --build to run whole Application
│   ├── frontend
│   │   ├── Dockerfile
│   │   ├── package.json
│   │   ├── package-lock.json
│   │   ├── public
│   │   │   ├── favicon.ico
│   │   │   ├── index.html
│   │   │   ├── logo192.png
│   │   │   ├── logo512.png
│   │   │   ├── manifest.json
│   │   │   └── robots.txt
│   │   ├── README.md
│   │   └── src
│   │       ├── api.js
│   │       ├── components
│   │       │   ├── App.js
│   │       │   ├── ButtonToggleSeparate.js
│   │       │   ├── ButtonToggleStats.js
│   │       │   ├── ButtonToggleSum.js
│   │       │   ├── TableSeparateAuthors.js
│   │       │   ├── TableSumWords.js
│   │       │   └── TableWordsStats.js
│   │       ├── index.css
│   │       ├── index.js
│   │       ├── reducers
│   │       │   └── reducer.js
│   │       ├── sagas
│   │       │   └── sagas.js
│   │       ├── setupTests.js
│   │       └── styles
│   │           └── styles.css
│   └── requirements.txt
└── unittest
    ├── Readme.md
    ├── testing_frontend.py
    └── testing_webscrapper.py

```

# To Do	
```
- [x] Store data in DB
- [x] Return json results on requests
    - [x] Make async mechanism for data gathering
- [x] Integrate Posgresql with Django models 

```

# Things that should be done
```
- [ ] Database lock when inserting new records
```
# Things that may improve efficiency

### WebScrapper

##### Faster scrapping
```
- [ ] Scrapy? 
```
##### Faster counting 
```
- [ ] Numpy (arrays of strings) counting ? 
```

### Database

##### Faster inserting to table
```
- [ ] one of this should make it faster
    - [ ] ? SQL insert Query
    - [ ] <del>Psycopg.copy_to</del>  
    - [ ] ? CELERY_RESULT_BACKEND
    - [x] Threading reduced time by 3.5 times
```
currently model Word is defined to make Word.save() <-- Solution: CELERY_RESULT_BACKEND ?

##### Decrease memory consumption
```
- [ ] Store every request instead of all at once to DB,  + better memory, - can be slower
    - [ ] modify records when word is repeated in db and sum values with next blog for same user 
```

### Docker-compose 

##### *.YML configuration

```
- [ ] Replace sleep with some checking functions (`depend on` fails if service restarts, and DB is restarting everytime)

- [ ] bash commands will probably not work in windows
```

# Frontend

### App runs in Dev mode
    Errors may show up even if cached.

### Application Url

http://localhost:3000/stats

### Rendering Tables

- Not optimal: Fetching every selected author after selection has changed.

### React-dom

Warning of deprecated function:

    `Warning: componentWillReceiveProps has been renamed, and is not recommended for use. See https://fb.me/react-unsafe-component-lifecycles for details.`